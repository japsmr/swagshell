#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "util.h"

void *malloc_safe(size_t sz) {
    void *p = malloc(sz);
    if(!p) {
        fprintf(stderr, "OUT OF MEMORY\n");
        exit(EXIT_FAILURE);
    }
    return p;
}
void *realloc_safe(void *p, size_t sz) {
    void *newptr = realloc(p, sz);
    if(!p) {
        fprintf(stderr, "OUT OF MEMORY\n");
        exit(EXIT_FAILURE);
    }
    return newptr;
}

void str_append_char_a(char **dest, char src, size_t *capacity) {
    size_t len = strnlen(*dest, *capacity);
    if(len >= *capacity - 1) {
        *dest = realloc_safe(*dest, (*capacity *= 2));
    }
    (*dest)[len] = src;
    (*dest)[len + 1] = '\0';
}

void str_append_str_a(char **dest, char *src, size_t *capacity) {
    size_t len = strlen(src) + strnlen(*dest, *capacity);

    if(len >= *capacity - 1) {
        *dest = realloc_safe(*dest, (*capacity *= 2));
    }

    strncat(*dest, src, strlen(src));
}
