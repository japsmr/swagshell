#include "swagshell.h"
#include "command.h"
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>

static char *line = NULL;
static size_t buffer_size = 0;
static ssize_t nread;

// TODO use $PS1 for prompt (eval and stuff)
#define PROMPT "$ "

void signal_handler(int signum) {
    if (signum == SIGINT) {
        fprintf(stdout, "\n");
        fprintf(stderr, PROMPT);
        // emulate an empty command (the user just pressing enter)
        *line = '\n';
    }
}

void interactive(void) {

    signal(SIGINT, signal_handler);

    fprintf(stderr, PROMPT);

    while ((nread = getline(&line, &buffer_size, stdin)) != -1) {
        cmd_run(line);
        fprintf(stderr, PROMPT);
    }

    free(line);
}
