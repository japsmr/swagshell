
#include "command.h"
#include "util.h"

void cmd_run(char *command) {
    Command cmd;

    // ignore empty command (NULL or "")
    if(!command || !command[0])
        return;

    cmd_parse(command, &cmd);

    // execute cmd

    NOT_IMPLEMENTED;
}
