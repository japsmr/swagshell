#ifndef SWAGSHELL_UTIL_H
#define SWAGSHELL_UTIL_H

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#define NOT_IMPLEMENTED assert("Not implemented" && 0)

#define ARR_SIZE(arr) (sizeof(arr)/sizeof(arr[0]))

void *malloc_safe(size_t sz);
void *realloc_safe(void *p, size_t sz);

// string utility
// suffix 'a' means reallocates to fit
void str_append_char_a(char **dest, char c, size_t *capacity);
void str_append_str_a(char **dest, char *src, size_t *capacity);

#endif
