#include "command.h"
#include "util.h"

typedef enum {
    TOKEN_UNKNOWN = 0,
    TOKEN_IDENTIFIER,
    TOKEN_STRING,
    TOKEN_OPERATOR,
    TOKEN_SPECIAL,
    TOKEN_EOF
} TokenType;

typedef struct {
    TokenType type;
    char *value;
} Token;

void cmd_parse(char *command, Command *dest) {
    (void) command;
    (void) dest;

    NOT_IMPLEMENTED;
}

