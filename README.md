# swagshell

this is the only shell with swag

## dependencies

gnu make

## build

make

## run

./build/swagsh

## install

sudo mv ./build/swagsh /usr/local/bin

## uninstall

sudo rm /usr/local/bin/swagsh

## license

agpl3
